VERSION = 0.1

ifdef SYSTEMROOT
	LIBS = -lpdcurses
	RM = rm -f tt.exe
	RM = rm -f ttx.exe
	CLOC = cloc-1.56 --quiet
else
	LIBS = -lcurses
	RM = rm -f tt
	RM = rm -f ttx
	CLOC = cloc --quiet
endif
	
all: sloc tt ttx

tt: clean
	gcc tt.c $(LIBS) -DVERSION=\"${VERSION}\" -std=c99 -Os -o tt
    
ttx: clean
	gcc ttx.c $(LIBS) -DVERSION=\"${VERSION}\" -std=c99 -Os -o ttx

sloc:
	$(CLOC) tt.c

tt.pdf:
	enscript tt.c --pretty-print=c --color=1 --no-header --output=- | ps2pdf - > tt.pdf
    
ttx.pdf:
	enscript tt.c --pretty-print=c --color=1 --no-header --output=- | ps2pdf - > ttx.pdf

clean:
	$(RM)

.PHONY: all sloc clean
