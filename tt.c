#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "curses.h"
#include <sys/timeb.h>

static const uint32_t tmnomask[4] = {0xf000,0xf00,0xf0,0xf}; /*mask each row*/
static uint16_t grid[21];                                    /*initialize bg*/
static const uint16_t tnmolib[7][4] = {
    {0x4444,0x0f00,0x4444,0x0f00},{0xcc00,0xcc00,0xcc00,0xcc00},/*I & O*/
    {0x6c00,0x8c40,0x6c00,0x8c40},{0xc600,0x4c80,0xc600,0x4c80},/*S & Z*/
    {0x4e00,0x4640,0xe40,0x4c40},{0x6440,0x8e00,0x44c0,0x0e20,},/*T & P*/
    {0xc440,0x0e80,0x4460,0x2e00}/*L*/
};

typedef struct tmno {
    unsigned int r:2; /*rotation value*/
    unsigned int t:3; /*type*/
    unsigned int x:4; /*x position (column)*/
    unsigned int y:5; /*y position (row)*/
} tmno;

static void drawbitr(uint16_t x,int i,int y){ /*draw a row of bits*/
    int cnt;
    static const int mask = 1 << 15;
    for(cnt=1;cnt<=16;++cnt){
        if((x & mask) != 0)mvaddch(i+y,cnt,ACS_CKBOARD);
        x <<= 1;
    }
}

static uint16_t getrow(tmno t, int i){
    return (tnmolib[t.t][t.r] & tmnomask[i]) << (i*4) >> (t.x);
}

static int rowsfree(tmno t){
    return (getrow(t,0) & grid[t.y + 0]) +(getrow(t,1) & grid[t.y + 1])+
        (getrow(t,2) & grid[t.y + 2])+(getrow(t,3) & grid[t.y + 3]);
}

static int time(){
    struct timeb t;
    ftime(&t);
    return t.time*1000 + t.millitm;
}

int main(void){
    tmno s,t = {0,0,5,0};
    int i,j=0,sm=0,score=0,st=time();
    for(i = 0; i < 21;i++) grid[i] = i < 20 ? 0x4002 : 0x7ffe;
    initscr();
    curs_set(0);
    keypad(stdscr, TRUE);
    while(j != 27){
        s=t;
        timeout(200 - (time() - st));
        switch(getch()){
        case KEY_RIGHT:
            t.x++;
            break;
        case KEY_LEFT:
            t.x--;
            break;
        case KEY_UP:
            t.r = t.r > 3 ? 0 : t.r + 1;
            break;
        case KEY_DOWN:
            t.y++;
        }
        if(!rowsfree(t) == 0)t=s;
        if((time() - st) >=  200){
            t.y++;
            if(!rowsfree(t) == 0){/*space is not available*/
                t.y--;
                if(t.y == 0){ /*tetramino is off the top of the board*/
                    timeout(-1);
                    mvprintw(22,1,"game over! press esc to quit, any key to start");
                    j=getch();/*wait for user input*/
                    for(i = 0; i < 21;i++)grid[i] = i < 20 ? 0x4002 : 0x7ffe;
                }else{ /*check for full rows*/
                    for(i=0; i<4; i++){
                        grid[t.y + i] |= getrow(t,i);
                        if((grid[t.y+i] == 0x7ffe) && (getrow(t,i) != 0)){
                            memmove(&grid[1],&grid[0],(t.y+i) * (sizeof(uint16_t)));
                            sm++; /*score multiplier*/
                        }
                    }
                    score += sm * sm;
                    sm=0; /*reset score multiplier*/
                }
                t.t = rand() % 7;
                t.y=0;
                t.x=5;
            }
            st=time();
        }
        clear();
        for(i=0; i<21; i++){
            drawbitr(grid[i],i,0);
            if(i<4)drawbitr(getrow(t,i),i,t.y);
        }
        mvprintw(22,1,"score: %d",score);
    }
    endwin();
    return 0;
}
/* vim:set ts=4 sw=4 expandtab: */
